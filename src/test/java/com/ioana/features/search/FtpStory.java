package com.ioana.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.ioana.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class FtpStory {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps steps;

//    @Issue("valid")
//    @Test
//    public void login_valid() {
//
//        steps.go_to_the_home_page();
//        steps.fill_data_and_login("linux.scs.ubbcluj.ro", "vvta", "VVTA2020");
//        steps.logout();
//    }

    @Test
    public void new_dir_button_is_clicked_and_a_new_folder_is_created_then_is_deleted() {

        steps.go_to_the_home_page();
        steps.fill_data_and_login("linux.scs.ubbcluj.ro", "vvta", "VVTA2020");

        steps.create_new_dir("tnci0214");
        steps.confirm_and_go_back();

        steps.select_and_delete_a_dir("tnci0214");
        steps.confirm_and_go_back();
        steps.logout();
    }


    //    @Test
//    public void searching_by_keyword_banana_should_display_the_corresponding_article() {
//        anna.is_the_home_page();
//        anna.looks_for("pear");
//        anna.should_see_definition("An edible fruit produced by the pear tree, similar to an apple but elongated towards the stem.");
//    }
//
//    @Pending @Test
//    public void searching_by_ambiguious_keyword_should_display_the_disambiguation_page() {
//    }
} 