package com.ioana.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import java.util.List;

public class UserDirPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"BrowseForm\"]/div")
    private WebElementFacade dirTree;

    @FindBy(xpath = "/html/body/div/div[2]/div/form/table[1]/tbody/tr/td/input[1]")
    private WebElementFacade newDirButton;

    @FindBy(id = "maintable")
    private WebElementFacade listOfDirectories;

    @FindBy(xpath = "/html/body/div/div[2]/div/form/table[1]/tbody/tr/td/div/input[3]")
    private WebElementFacade deleteButton;

    @FindBy(xpath = "//*[@id=\"StatusbarForm\"]/a[4]")
    private WebElementFacade logoutButton;


    public String getString() {

        return dirTree.getValue();
    }

    public void click_new_dir_button() {

        newDirButton.click();
    }

    private void click_element_by_attribute_value(WebElementFacade el, String attribute, String value) {

        List<WebElement> list = el.findElements(By.tagName("input"));

        for (int i = 0; i < list.size(); i++) {

            String dir = list.get(i).getAttribute(attribute);

            if (dir.equalsIgnoreCase(value)) {

                list.get(i).click();
                break;
            }
        }
    }

    public void check_dir_to_delete(String directory) {

        click_element_by_attribute_value(listOfDirectories, "value", directory);
    }

    public void click_delete() {

        deleteButton.click();
    }

    public void logout() {

        logoutButton.click();
    }

}