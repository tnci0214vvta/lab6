package com.ioana.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/index.php")
public class FtpPage extends PageObject {

    @FindBy(name="ftpserver")
    private WebElementFacade comboFtpServer;

    @FindBy(name="username")
    private WebElementFacade textName;

    @FindBy(name="password")
    private WebElementFacade textPassword;

    @FindBy(id="LoginButton1")
    private WebElementFacade loginButton;

    @FindBy(xpath="//*[@id=\"smallbutton\"]")
    private WebElementFacade newDirButton;


    public void select_ftpServer(String server) {

        comboFtpServer.selectByValue(server);
    }

    public void enter_textName(String name) {

        textName.type(name);
    }

    public void enter_textPassword(String password) {

        textPassword.type(password);
    }

    public void click_login() {

        loginButton.click();
    }

    public void select_newDirButton() {

        newDirButton.click();
    }



//    public List<String> getDefinitions() {
//        WebElementFacade definitionList = find(By.tagName("ol"));
//        return definitionList.findElements(By.tagName("li")).stream()
//                .map( element -> element.getText() )
//                .collect(Collectors.toList());
//    }
}