package com.ioana.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class CreateNewDirPage extends PageObject {

    @FindBy(name="newNames[1]")
    private WebElementFacade new_dir_name;

    @FindBy(xpath="/html/body/div/div[2]/div/div[2]/form/a[2]")
    private WebElementFacade submitButton;

    //@FindBy(xpath="//*[@id=\"NewDirForm\"]/a")
    @FindBy(xpath="/html/body/div/div[2]/div/div[2]/form/a")
    private WebElementFacade backButton;


    public void enter_dir_name(String name) {

        new_dir_name.type(name);
    }

    public void click_submit() {

        submitButton.click();
    }

    public void click_back() {

        backButton.click();
    }

}