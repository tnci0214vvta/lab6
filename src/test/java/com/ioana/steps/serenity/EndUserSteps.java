package com.ioana.steps.serenity;

import com.ioana.pages.CreateNewDirPage;
import com.ioana.pages.FtpPage;
import com.ioana.pages.UserDirPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    FtpPage ftpPage;
    UserDirPage userDirPage;
    CreateNewDirPage newDirPage;

    @Step
    public void go_to_the_home_page() {

        ftpPage.open();
        ftpPage.click_login();
    }

    @Step
    public void fill_data_and_login(String server, String name, String pass) {

        ftpPage.select_ftpServer(server);
        ftpPage.enter_textName(name);
        ftpPage.enter_textPassword(pass);

        ftpPage.click_login();
    }

    @Step
    public void should_see_user(String name) {

        assertThat(userDirPage.getString(), userDirPage.getString().contains("name"));

    }

    @Step
    public void create_new_dir(String name) {

        userDirPage.click_new_dir_button();
        newDirPage.enter_dir_name(name);
    }

    @Step
    public void confirm_and_go_back() {

        newDirPage.click_submit();
        newDirPage.click_back();

    }

    @Step
    public void select_and_delete_a_dir(String directory) {

        userDirPage.check_dir_to_delete(directory);
        userDirPage.click_delete();
    }

    @Step
    public void logout() {

        userDirPage.logout();
    }


    //    @Step
//    public void enters(String keyword) {
//        dictionaryPage.enter_keywords(keyword);
//    }
//
//    @Step
//    public void starts_search() {
//        dictionaryPage.lookup_terms();
//    }
//
//    @Step
//    public void should_see_definition(String definition) {
//        assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition)));
//    }
//
//    @Step
//    public void is_the_home_page() {
//        dictionaryPage.open();
//    }
//
//    @Step
//    public void looks_for(String term) {
//        enters(term);
//        starts_search();
//    }
}